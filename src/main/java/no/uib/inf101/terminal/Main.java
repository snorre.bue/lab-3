package no.uib.inf101.terminal;

public class Main {

  public static void main(String[] args) {
    // Create a new shell
    SimpleShell shell = new SimpleShell();
    shell.installCommand(new CmdEcho());

    // Run the shell in the terminal GUI
    // commit test comment
    Terminal gui = new Terminal(shell);
    gui.run();
  }
}